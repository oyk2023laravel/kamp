<?php

namespace App\Providers;

use App\Concrete\DemirCivi;
use App\Concrete\IzeltasCekic5850;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class CarpenterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(Nail::class, function () {
            return new DemirCivi;
        });

        $this->app->singleton(Hammer::class, function (Application $app) {
            return new IzeltasCekic5850($app->make('session'));
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
