<?php

namespace App\Providers;

use Illuminate\Foundation\Http\HtmlDumper;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\VarDumper\Caster\ReflectionCaster;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\VarDumper;

class VarDumperLightThemeProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $basePath = $this->app->basePath();
        $compiledViewPath = $this->app['config']->get('view.compiled');

        $dumper = new HtmlDumper($basePath, $compiledViewPath);
        $dumper->setTheme('light');

        $cloner = tap(new VarCloner())->addCasters(ReflectionCaster::UNSET_CLOSURE_FILE_INFO);

        VarDumper::setHandler(fn ($value) => $dumper->dumpWithSource($cloner->cloneVar($value)));
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
