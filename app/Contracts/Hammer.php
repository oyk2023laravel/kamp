<?php

namespace App\Contracts;

interface Hammer
{
    // Nail to the specific point
    public function nailing(Nail $nail, string $point);
}