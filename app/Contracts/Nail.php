<?php

namespace App\Contracts;

interface Nail
{
    // Get name of the nail
    public function getName(): string;

    // Stick nail to the specific point
    public function stick(string $point): void;
}