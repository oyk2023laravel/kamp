<?php

namespace App\Concrete;
use App\Contracts\Nail;

class DemirCivi implements Nail
{
    // Get name of the nail
    public function getName(): string
    {
        return "Demir Çivi";
    }

    // Stick nail to the specific point
    public function stick(string $point): void
    {
        echo $this->getName() . " çivisi " . $point . " noktasına tutturuluyor.<br>";
    }
}