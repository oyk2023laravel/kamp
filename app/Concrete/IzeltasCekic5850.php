<?php

namespace App\Concrete;
use App\Contracts\Hammer;
use App\Contracts\Nail;
use Illuminate\Session\SessionManager;

class IzeltasCekic5850 implements Hammer
{
    private SessionManager $session;

    public function __construct(SessionManager $session)
    {
        $this->session = $session;
    }

    public function nailing(Nail $nail, string $point)
    {
        $this->session->put('izeltas', ($this->kirilmaDurumu())+1);

        if ($this->kirilmaDurumu() == 10)
        {
            echo "Izeltas çekici kırıldığı için kullanılamadı!";
        }
        else
        {
            echo "Izeltas çekici ile " . $nail->getName() . " çivisi " . $point . " noktasına çakılıyor.<br>";
            echo "Çekicin durumu: " . $this->kirilmaDurumu() . "<br>";
        }
    }

    private function kirilmaDurumu()
    {
        return $this->session->get('izeltas');
    }
}